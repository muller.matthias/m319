# mein Portfolio

## Block 1

### Was ist mein Ziel heute?
Mich in CodeBlocks zu vertiefen. 

### Was habe ich heute gelernt?
Ich lernte was ein Nassi Schneiderman Diagramm ist und lernten das Programm CodeBlocks kennen. 

### Was bereitet mir noch Schwierigkeiten?
Die richtigen Tastenkombinationen finden.

### Wie löse ich diese?
Ich löse sie in dem ich versuche die Tastenkombinationen möglichst oft anzuwenden, damit sie dann in meinem Gehirn hängen bleiben.


-------------
------------



# Programmablaufplan

## Kontrollstrukturen


### Bedingte Anweisungen
if, switch

### Schleifen
while, do, for

### Sprunganweisungen
break, continue, goto, return



# Generelle Informationen

## Programmablauflan 

### Formen

Start: Rund
Bedinung (Ja / Nein): Diamantförmig
"Weitermachen" : Rechteck
Ende / Stop: Rund


# Nassi-Schneiderman-Diagramm

## Informationen
Stellt das Programm als hierarchische Lösung eines Problems dar, das in Teilprobleme zerlegt wird. 

Programme werden als Blöcke dargestellt und in Teilblöcke unterteilt.

Auch bekannt als Struktogramm

## ELement
 
Block = Anweisung 

Unterblock = Block in Block

Verzweigter Block = Bedingung

## WIkipedia Elemente Hilfe
https://de.wikipedia.org/wiki/Programmablaufplan
